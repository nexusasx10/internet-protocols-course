import socket


HOST = "127.0.0.1"
PORT = 4343


class Client:
    def __init__(self):
        self.socket = socket.create_connection((HOST, PORT))
        self.buffer = ''

    def start(self):
        while True:
            self.buffer = self.socket.recv(1024).decode()
            print('Message from server: {}'.format(self.buffer))
            if self.buffer == 'Bye':
                self.socket.sendall(b'Bye')
                self.socket.close()
                break


if __name__ == "__main__":
    Client().start()
