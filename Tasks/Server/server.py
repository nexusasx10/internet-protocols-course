import socket
import threading
import time
from functools import partial


class Server:
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(('127.0.0.1', 4343))
        self.clients = {}

    def start(self):
        self.socket.listen(1)
        print('Server started')
        self._connection_cycle()

    def _connection_cycle(self):
        while True:
            connection, address = self.socket.accept()
            self.clients[connection] = address
            print('Connection from {}'.format(address))
            thread = threading.Thread(target=partial(self._client_cycle, client=connection))
            thread.start()

    def _client_cycle(self, client):
        client.sendall(b'Hi')
        for _ in range(10):
            client.sendall(b'some message')
            time.sleep(1)
        client.sendall(b'Bye')
        if client.recv(1024) == b'Bye':
            client.shutdown(socket.SHUT_RDWR)
            client.close()
            print('{} was disconnected'.format(self.clients[client]))


if __name__ == "__main__":
    Server().start()
