#! /usr/bin/env python3
import socket
import time
import logging
import sys

from concurrent.futures import ThreadPoolExecutor
from select import select
from argparse import ArgumentParser
from threading import Thread

import datetime

import ntplib

SNTP_PORT = 123
EPOCH_DELTA = (
    datetime.date(1900, 1, 1) - datetime.date(*time.gmtime(0)[:3])
).days * 24 * 3600


class SNTP:
    def __init__(self, args):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.args = args
        self.stratum = 16
        self.last_update = 0
        self.working = False

    def start(self):
        self.socket.bind(('0.0.0.0', self.args.port))
        self.working = True
        logging.info('Server started')
        Thread(target=self.receive_cycle).start()
        while self.working:
            command = input('> ')
            self.parse_command(command)

    def stop(self):
        self.working = False
        logging.info('Server stopped')
        self.socket.close()

    def parse_command(self, command):
        if command == '':
            return
        elif command == 'stop':
            self.stop()
        else:
            print('Unknown command')

    def receive_cycle(self):
        with ThreadPoolExecutor() as thread_pool:
            while self.working:
                if select([self.socket], [], []):
                    data, client = self.socket.recvfrom(1024)
                    r_time = time.time()
                    logging.info('Request from {}'.format(client[0]))
                    thread_pool.submit(self.send, data, client, r_time)

    def send(self, data, client, r_time):
        packet = NTPPacket.from_bytes(data)
        packet.mode = 4
        packet.receive_timestamp = r_time
        packet.transmit_timestamp = time.time()
        self.socket.sendto(packet.to_bytes(), client)

    def synchronize(self):
        packet = NTPPacket(mode=3, originate_timestamp=time.time())
        self.socket.sendto(packet.to_bytes(), self.args.server)


class NTPPacket:
    byte_order = 'big'

    def __init__(
        self, leap_indicator=0, version=4, mode=4, stratum=0, poll_interval=0,
        precision=0, root_delay=0, root_dispersion=0, reference_id=0,
        reference_timestamp=0, originate_timestamp=0,
        receive_timestamp=0, transmit_timestamp=0,
        destination_timestamp=0
    ):
        self.leap_indicator = leap_indicator
        self.version = version
        self.mode = mode
        self.stratum = stratum
        self.poll_interval = poll_interval
        self.precision = precision
        self.root_delay = root_delay
        self.root_dispersion = root_dispersion
        self.reference_id = reference_id
        self.reference_timestamp = reference_timestamp
        self.originate_timestamp = originate_timestamp
        self.receive_timestamp = receive_timestamp
        self.transmit_timestamp = transmit_timestamp
        self.destination_timestamp = destination_timestamp

    def to_bytes(self):
        data = b''
        data += (
            self.leap_indicator << 6 | self.version << 3 | self.mode
        ).to_bytes(1, self.byte_order)
        data += self.stratum.to_bytes(1, self.byte_order)
        data += self.poll_interval.to_bytes(1, self.byte_order)
        data += self.precision.to_bytes(1, self.byte_order)
        data += self.root_delay.to_bytes(4, self.byte_order)
        data += self.root_dispersion.to_bytes(4, self.byte_order)
        data += self.reference_id.to_bytes(4, self.byte_order)
        data += self.time_to_bytes(self.reference_timestamp)
        data += self.time_to_bytes(self.originate_timestamp)
        data += self.time_to_bytes(self.receive_timestamp)
        data += self.time_to_bytes(self.transmit_timestamp)
        return data

    @classmethod
    def from_bytes(cls, data):
        return NTPPacket(
            leap_indicator=int.from_bytes(data[0:1], cls.byte_order) >> 6 & 0x3,
            version=int.from_bytes(data[0:1], cls.byte_order) >> 3 & 0x7,
            mode=int.from_bytes(data[0:1], cls.byte_order) & 0x7,
            stratum=int.from_bytes(data[1:2], cls.byte_order),
            poll_interval=int.from_bytes(data[2:3], cls.byte_order),
            precision=int.from_bytes(data[3:4], cls.byte_order),
            root_delay=int.from_bytes(data[4:8], cls.byte_order),
            root_dispersion=int.from_bytes(data[8:12], cls.byte_order),
            reference_id=int.from_bytes(data[12:16], cls.byte_order),
            reference_timestamp=cls.bytes_to_time(data[16:24]),
            originate_timestamp=cls.bytes_to_time(data[24:32]),
            receive_timestamp=cls.bytes_to_time(data[32:40]),
            transmit_timestamp=cls.bytes_to_time(data[40:48])
        )

    def target_time(self):
        delta = (
            (self.destination_timestamp - self.originate_timestamp) -
            (self.transmit_timestamp - self.receive_timestamp)
        )
        return self.transmit_timestamp + delta/2

    @classmethod
    def time_to_bytes(cls, t):
        t += EPOCH_DELTA
        integral = int(t)
        fractional = int((t - integral) * 2**32)
        integral = integral.to_bytes(4, cls.byte_order)
        fractional = fractional.to_bytes(4, cls.byte_order)
        return integral + fractional

    @classmethod
    def bytes_to_time(cls, b):
        integral = int.from_bytes(b[0:4], cls.byte_order)
        fractional = float(int.from_bytes(b[4:8], cls.byte_order)) / 2**32
        return integral + fractional - EPOCH_DELTA


def parse_args():
    parser = ArgumentParser(description='SNTP Server')
    parser.add_argument('-d', '--delay', metavar='N', type=int, default=0)
    parser.add_argument('-p', '--port', metavar='N', type=int, default=SNTP_PORT)
    parser.add_argument('--logging', help='write logs to the file', action='store_true')
    return parser.parse_args()


def logging_config(args):
    handlers = [logging.StreamHandler(sys.stdout)]
    if args.logging:
        handlers.append(logging.FileHandler('sntp_session.log'))
    logging.basicConfig(
        handlers=handlers,
        level=logging.INFO,
        format='[%(asctime)s] %(message)s',
        datefmt='%m.%d.%Y %H:%M:%S'
    )


def main():
    args = parse_args()
    args.server = 'ru.pool.ntp.org'
    logging_config(args)
    server = SNTP(args)
    server.start()


if __name__ == '__main__':
    p = ntplib.NTPClient().request('ru.pool.ntp.org', 4, 123, 5)
    main()
