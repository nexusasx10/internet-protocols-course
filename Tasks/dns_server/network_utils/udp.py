import socket

import time

from thread_pool import ThreadPool


class UDPConnection:
    """"""
    def __init__(self, address, buffer_size=2048):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.address = address
        self.buffer_size = buffer_size

    def send(self, data):
        self.socket.sendto(data, self.address)

    def receive(self):
        return self.socket.recvfrom(self.buffer_size)

    def close(self):
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()


class UDPServer(UDPConnection):
    """"""
    def __init__(self, listen_to, buffer_size=2048):
        super().__init__(listen_to, buffer_size)
        self.running = False

    def run(self):
        self.socket.bind(self.address)
        self.running = True

    def stop(self):
        self.running = False
        self.close()


class UDPClient(UDPConnection):
    """"""
