import socket
from select import select

from thread_pool import ThreadPool


class TCPConnection:
    """"""
    def __init__(self, address):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.address = address

    def send(self, data):
        self.socket.send(data)

    def receive(self):
        result = b''
        while select([self.socket], [], [], 0.25)[0]:
            data = self.socket.recv(1024)
            if len(data) == 0:
                break
            result += data
        return result

    def close(self):
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()


class TCPServer(TCPConnection):
    """"""
    def __init__(self, listen_to):
        super().__init__(listen_to)
        self.running = False

    def run(self):
        self.socket.bind(self.address)
        self.socket.listen(1)
        self.running = True

    def stop(self):
        self.running = False
        self.close()

    def connecting_cycle(self):
        self.socket.settimeout(0.2)
        while self.running:
            try:
                client, address = self.socket.accept()
            except OSError:
                continue
            self.handle_client(client, address)

    def handle_client(self, client, address):
        pass


class TCPClient(TCPConnection):
    """"""
    def __enter__(self):
        self.socket.connect(self.address)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
