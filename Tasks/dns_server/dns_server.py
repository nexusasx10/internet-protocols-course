from argparse import ArgumentParser

import re
from enum import Enum

from network_utils.tcp import TCPServer
from network_utils.udp import UDPServer


class DNSServer:
    def __init__(self, args):
        self.udp_server = UDPServer(('0.0.0.0', args.port))
        self.tcp_server = TCPServer(('0.0.0.0', args.port))
        self.args = args


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', metavar='N', help='port to listen', default=53)
    parser.add_argument('-f', '--file', help='zone file', dest='sender', default='')
    parser.add_argument('--logging', help='write logs to the file', action='store_true')
    return parser.parse_args()


class DNSZone:
    class RRType(Enum):
        A = 'A'
        AAAA = 'AAAA'
        CNAME = 'CNAME'
        MX = 'MX'
        NS = 'NS'
        SOA = 'SOA'

    class RRClass(Enum):
        IN = 'IN'
        CS = 'CS'
        CH = 'CH'
        HS = 'HS'

    def __init__(self):
        self.entries = []

    @staticmethod
    def _check_brackets(text):
        open_count = 0
        for char in text:
            if char == '(':
                open_count += 1
            elif char == ')':
                open_count -= 1
            if open_count < 0:
                return False
        return open_count == 0

    def parse_zone(self, data):
        def optional(string):
            return '(' + string + ')?'
        ttl = r'\d+'
        class_ = '|'.join((c.value for c in self.RRClass))
        type_ = '.+'
        blank = r' '
        comment = r';.*'
        resource_record = '(' + blank.join((optional(ttl), optional(class_), type_, '.+')) + '|' + blank.join((optional(class_), optional(ttl), type_, '.+')) + ')'
        domain_name = r'\w+(\.\w+)*\.?'
        file_name = r'.+'

        line_regs = [
            blank + optional(comment),
            blank.join((r'$ORIGIN', domain_name, optional(comment))),
            blank.join((r'$INCLUDE', file_name, optional(domain_name), optional(comment))),
            blank.join((domain_name, resource_record, optional(comment))),
            blank.join(('', resource_record, optional(comment)))
        ]
        l = data.split('\n')
        buffer = ''
        r = []
        has_next = False
        for s in l:
            s = re.sub('[\t ]+', ' ', s)
            buffer += s
            if '(' in s:
                has_next = True
            if ')' in s:
                has_next = False
            if not has_next:
                r.append(buffer)
                buffer = ''
        for line in r:
            for reg in line_regs:
                match = re.fullmatch(reg, line)
                if match:
                    print(match.end())


def main():
    args = parse_args()
    DNSServer(args)


if __name__ == '__main__':
    main()

with open('C:/users/andre/desktop/1.txt', encoding='utf-8') as fd:
    print(DNSZone().parse_zone(fd.read()))
