import os
from queue import Queue, Empty as QueueEmptyError
from threading import Thread


class ThreadPool:
    """"""
    def __init__(self, start_size=os.cpu_count()):
        self._executors = []
        self._tasks = Queue()
        self._start_size = start_size
        for i in range(self._start_size):
            executor = self.Executor(self._tasks)
            self._executors.append(executor)
        self.running = False

    def run(self):
        for executor in self._executors:
            executor.run()
        self.running = True

    def stop(self, wait=True):
        for executor in self._executors:
            executor.stop()
            if wait:
                executor.thread.join()
        self.running = False

    def __enter__(self):
        self.run()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def submit(self, task, *args, **kwargs):
        future = self.Future(task, args, kwargs)
        self._tasks.put(future)
        return future

    class Future:
        def __init__(self, task, args, kwargs):
            self.task = task
            self.args = args
            self.kwargs = kwargs
            self.done = False
            self.running = False
            self.result = None
            self.exception = None

        def run(self):
            try:
                self.running = True
                self.result = self.task(*self.args, **self.kwargs)
                self.running = False
            except BaseException as exception:
                self.exception = exception
                self.running = False
                return
            self.done = True

    class Executor:
        def __init__(self, tasks):
            self.tasks = tasks
            self.thread = Thread(target=self.running_cycle, daemon=True)
            self.running = False

        def run(self):
            self.running = True
            self.thread.start()

        def stop(self):
            self.running = False

        def running_cycle(self):
            while self.running:
                task = None
                while not task and self.running:
                    try:
                        task = self.tasks.get(timeout=0.1)
                    except QueueEmptyError:
                        continue
                if task:
                    task.run()
