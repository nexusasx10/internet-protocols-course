

class Base64EncodeError(Exception):
    pass


class Base64DecodeError(Exception):
    pass


class Base64:

    _chars = b'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
             b'abcdefghijklmnopqrstuvwxyz' \
             b'0123456789+/'
    _filler = b'='

    @classmethod
    def _get_chars(cls, bytes_, miss):
        result = b''
        for byte in bytes_:
            result += bytes((cls._chars[byte],))
        if miss > 0:
            result = result[:-miss] + cls._filler * miss
        return result

    @classmethod
    def _get_bytes(cls, chars):
        result = []
        miss = 0
        for char in chars:
            if bytes((char,)) == cls._filler:
                miss += 1
                result.append(0)
                continue
            result.append(Base64._chars.find(bytes((char,))))
        return result, miss

    @staticmethod
    def _get_window(byte, start=0, end=8, shift=None):
        start = 8 - start
        end = 8 - end
        if shift is None:
            shift = end
        mask = int((2 ** start - 1) - (2 ** end - 1))
        if shift > 0:
            return (byte & mask) >> shift
        else:
            return (byte & mask) << -shift

    @staticmethod
    def encode(data, result_in_bytes=False):
        if isinstance(data, str):
            data = data.encode()
        elif not isinstance(data, bytes):
            raise Base64EncodeError('Data must be str or bytes')
        result_buffer = ()
        miss = (3 - (len(data) % 3)) % 3
        for i in range(miss):
            data += bytes((0,))
        for i in range(len(data) // 3):
            t_buffer = data[3 * i], data[3 * i + 1], data[3 * i + 2]
            q_buffer = (
                Base64._get_window(t_buffer[0], 0, 6),
                Base64._get_window(t_buffer[0], 6, 8, -4) + Base64._get_window(t_buffer[1], 0, 4),
                Base64._get_window(t_buffer[1], 4, 8, -2) + Base64._get_window(t_buffer[2], 0, 2),
                Base64._get_window(t_buffer[2], 2, 8)
            )
            result_buffer += q_buffer
        result = Base64._get_chars(result_buffer, miss)
        if result_in_bytes:
            return result
        else:
            return result.decode()

    @staticmethod
    def decode(data, result_in_bytes=False):
        if isinstance(data, str):
            data = data.encode()
        elif not isinstance(data, bytes):
            raise Base64DecodeError('Data must be str or bytes')
        result_buffer = ()
        bytes_, miss = Base64._get_bytes(data)
        for i in range(len(bytes_) // 4):
            t_buffer = bytes_[4*i], bytes_[4*i + 1], bytes_[4*i + 2], bytes_[4*i + 3]
            q_buffer = (
                Base64._get_window(t_buffer[0], 2, 8, -2) + Base64._get_window(t_buffer[1], 2, 4),
                Base64._get_window(t_buffer[1], 4, 8, -4) + Base64._get_window(t_buffer[2], 2, 6),
                Base64._get_window(t_buffer[2], 6, 8, -6) + Base64._get_window(t_buffer[3], 2, 8)
            )
            result_buffer += q_buffer
        if miss > 0:
            result = bytes(result_buffer[:-miss])
        else:
            result = bytes(result_buffer)
        if result_in_bytes:
            return result
        else:
            return result.decode()
