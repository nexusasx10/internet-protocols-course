#! /usr/bin/env python3
import os
import sys
import getpass
import random
import socket
import ssl
import string
import re
import logging
from argparse import ArgumentParser

from utils.my_base64 import Base64


IMG_MIME = {
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.png': 'image/png',
    '.gif': 'image/gif'
}
LINE_SEPARATOR = '\r\n'
VALID_SYMBOLS = string.ascii_letters + string.digits
SMTP_PORT = 25
SMTP_SSL_PORT = 465


class SMTPError(Exception):
    pass


class SMTP:
    def __init__(self, args):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(0.5)
        self.args = args
        self.extensions = None

    def __enter__(self):
        self.socket.connect(self.args.server)
        code, data = self.receive()
        if code != 220:
            logging.info('Server not available')  # TODO
        else:
            logging.info('Connection established')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # self.socket.shutdown(socket.SHUT_RDWR) TODO
        self.socket.close()
        logging.info('Connection closed')

    def send(self, message, hide=False):
        if not hide:
            logging.info('> ' + message)
        try:
            self.socket.send((message + LINE_SEPARATOR).encode())
        except IOError:
            raise SMTPError()

    def receive(self, hide=False):
        message = ''
        buffer_size = 512
        received = self.socket.recv(buffer_size).decode()
        while True:
            message += received
            if len(received) < buffer_size:
                break
            try:
                received = self.socket.recv(buffer_size).decode()
            except socket.timeout:
                break
        if not hide:
            logging.info(message)
        lines = message.split(LINE_SEPARATOR)[:-1]
        last_line = lines[-1].split(maxsplit=1)
        code = int(last_line[0])
        if len(lines) > 1:
            lines = list(
                map(lambda l: l.split('-', maxsplit=1)[1], lines[:-1])
            )
            lines.append(last_line[1])
            return code, lines
        else:
            return code, last_line[1]

    def message(self, message, hide=False):
        self.send(message, hide)
        return self.receive(hide)

    def quit(self):
        self.send('QUIT')

    def ehlo(self):
        host = socket.gethostname()
        for s in host:
            if s not in VALID_SYMBOLS:
                host = socket.gethostbyname(socket.gethostname())
        code, data = self.message('EHLO {}'.format(host))
        if code != 250:
            print()  # TODO
            self.quit()
            return
        self.extensions = list(map(lambda x: x.split()[0], data))[1:]

    def starttls(self):
        if 'STARTTLS' not in self.extensions:
            print('Server does not support starttls')
            return
        code, data = self.message('STARTTLS')
        if code != 220:
            print()  # TODO
            return
        self.socket = ssl.wrap_socket(
            self.socket, ssl_version=ssl.PROTOCOL_SSLv23
        )

    def auth_login(self):
        # if 'AUTH' not in self.extensions:
        #     print('Server does not support authorisation')
        #     return
        code, data = self.message('AUTH LOGIN')
        if code != 334:
            print()  # TODO
            self.quit()
        login = input('Login: ')
        code, data = self.message(Base64.encode(login))
        if code != 334:
            print()  # TODO
            self.quit()
        password = getpass.getpass()
        code, data = self.message(Base64.encode(password))
        if code != 235:
            print()  # TODO
            self.quit()

    def mail_from(self):
        code, data = self.message('MAIL FROM: {}'.format(self.args.sender))
        if code != 250:
            print()  # TODO
            self.quit()

    def rcpt_to(self):
        code, data = self.message('RCPT TO: {}'.format(self.args.receiver))
        if code != 250:
            print()  # TODO
            self.quit()

    def data(self, message):
        code, data = self.message('DATA')
        if code != 354:
            print()  # TODO
            self.quit()
        code, data = self.message(message)
        if code != 250:
            print()  # TODO
            self.quit()


def parse_code(code):
    code = str(code)
    first = int(code[2])
    second = int(code[1])
    if first == 5:
        if second == 0:
            print('Syntax error')


class ImgSender:
    @classmethod
    def get_boundary(cls):
        return ''.join([random.choice(VALID_SYMBOLS) for _ in range(20)])

    @classmethod
    def get_images(cls, dir_path):
        try:
            file_paths = os.listdir(dir_path)
        except IOError:
            return
        for file_path in file_paths:
            file_path = os.path.join(dir_path, file_path)
            if os.path.isfile(file_path) and os.path.splitext(file_path)[1] in IMG_MIME:
                try:
                    with open(file_path, 'rb') as fd:
                        data = fd.read()
                        yield Base64.encode(data)
                except IOError:
                    continue

    @classmethod
    def build_html_image_part(cls, directory):
        html_part = []
        image_part = []
        for number, image in enumerate(cls.get_images(directory)):
            html_part.append('<img src="cid:image-{}">'.format(number))
            image_part.append(
                LINE_SEPARATOR.join(
                    [
                        '--{boundary}',
                        'Content-Type: image/jpeg',
                        'Content-Transfer-Encoding: base64',
                        'Content-Disposition: attachment; filename="test"',
                        'Content-Id: image-{}'.format(number),
                        '',
                        image
                    ]
                )
            )
        if len(html_part) == 0:
            return None, None
        html_part = LINE_SEPARATOR.join(html_part)
        image_part = LINE_SEPARATOR.join(image_part)
        return html_part, image_part

    @staticmethod
    def build_message_base(sender, receiver, subject):
        lines = [
            'From: {}'.format(sender),
            'To: {}'.format(receiver),
            'Subject: {}'.format(subject),
            'Content-Type: multipart/mixed; boundary={boundary}',
            '',
            '--{boundary}',
            'Content-Type: text/html; charset=utf-8',
            '',
            '{html_code}',
            '{images}',
            '--{boundary}--',
            '.',
            ''
        ]
        return '\r\n'.join(lines)

    @classmethod
    def build_message(cls, sender, receiver, subject, directory):
        html_code, images = cls.build_html_image_part(directory)
        base = cls.build_message_base(sender, receiver, subject)
        message = base.format(
            boundary='{boundary}',
            html_code=html_code,
            images=images
        )
        first = True
        boundary = None
        while first or boundary in message:
            first = False
            boundary = cls.get_boundary()
        message = message.format(boundary=boundary)
        return message

    @classmethod
    def send(cls, args):
        message = cls.build_message(args.sender, args.receiver, args.subject, args.directory)
        with SMTP(args) as smtp:
            smtp.ehlo()
            smtp.starttls()
            smtp.auth_login()
            smtp.mail_from()
            smtp.rcpt_to()
            smtp.data(message)
            smtp.quit()


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--server', metavar='ADDRESS[:PORT]', help='address of SMTP server', required=True)
    parser.add_argument('-t', '--to', help='receiver address', dest='receiver', required=True)
    parser.add_argument('-f', '--from', help='sender address', dest='sender', default='')
    parser.add_argument('--subject', help='message subject', default='Happy Pictures')
    parser.add_argument('-d', '--directory', help='picture directory', default='.')
    parser.add_argument('--ssl', help='allow ssl usage', action='store_true')
    parser.add_argument('--auth', help='authorization request', action='store_true')
    parser.add_argument('-v', '--verbose', help='display commands', action='store_true')
    parser.add_argument('--logging', help='write logs to the file', action='store_true')
    args = parser.parse_args()
    args.server = parse_server(args.server)
    args.receiver = parse_address(args.receiver)
    args.sender = parse_address(args.sender)
    parse_address(args)
    return args


def parse_server(server):
    server = tuple(server.split(':'))
    if len(server) == 1:
        return server[0], SMTP_PORT
    elif len(server) == 2:
        return server
    else:
        print('Bad server format')
        return


def logging_config(args):
    handlers = []
    if args.verbose:
        handlers.append(logging.StreamHandler(sys.stdout))
    if args.logging:
        handlers.append(logging.FileHandler('sntp_session.log'))
    if not handlers:
        handlers.append(logging.NullHandler())
    logging.basicConfig(
        handlers=handlers,
        level=logging.INFO,
        format='[%(asctime)s] %(message)s',
        datefmt='%m.%d.%Y %H:%M:%S'
    )


def parse_address(address):
    if re.fullmatch(r'[^<>]*<[^<>]+@[^<>]+>', address):
        return address
    elif re.fullmatch(r'[^<>]+@[^<>]+', address):
        return '<{}>'.format(address)
    else:
        print('Bad address format')
        return


def main():
    class A:
        pass
    #args = parse_args()
    args = A()
    args.__dict__ = {
        'server': ('smtp.yandex.ru', 587),
        'verbose': False,
        'logging': False,
        'sender': '<andrey.sheludyakov@yandex.ru>',
        'receiver': '<zoya.sheludyakova@mail.ru>',
        'subject': 'pics',
        'directory': 'img'
    }
    logging_config(args)
    ImgSender.send(args)


if __name__ == '__main__':
    main()
