import socket
import ssl
import getpass

from utils.my_base64 import Base64


def main():
    sock = Sock(socket.socket())
    sock.connect(('smtp.yandex.ru', 587))
    sock.writeln2('EHLO LERA')
    sock.writeln2('STARTTLS')
    sock.starttls()
    sock.writeln2('AUTH LOGIN')
    login = input('Login: ')
    sock.writeln2(Base64.encode(login).decode())
    password = getpass.getpass()
    sock.writeln2(Base64.encode(password).decode())
    sock.writeln2('MAIL FROM: <{}>', 'andrey.sheludyakov@yandex.ru')
    sock.writeln2('RCPT TO: <{}>', 'zoya.sheludyakova@mail.ru')
    sock.writeln2('DATA')
    message = build_message_base()
    sock.writeln2(message)
    sock.sock.close()


def build_message_base():
    lines = [
        'From: <andrey.sheludyakov@yandex.ru>',
        'To: <zoya.sheludyakova@mail.ru>',
        'Subject: subject',
        'Content-Type: multipart/mixed; boundary=boundary',
        '',
        '--boundary',
        'Content-Type: text/html; charset=utf-8',
        '',
        '<h1>hello</h1>',
        '<img src="cid:attachedImage"/>',
        '--boundary',
        'Content-Type: image/png',
        'Content-Transfer-Encoding: base64',
        'Content-Disposition: attachment;',
        'filename = "test.png"',
        'Content-Id: attachedImage',
        '',
        'iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAYAAADgbT9oAAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH2QYFDDsOk2vZjgAAACpJREFUOI1j/P///38GGgAmWhg6ajAKYGRgGI28oWow438GhtHIG6IGAwBk1ggU2mMOxgAAAABJRU5ErkJggg==',
        '--boundary--',
        '.',
        '',


    ]
    return '\r\n'.join(lines)


class Sock:
    def __init__(self, sock):
        self.sock = sock

    def connect(self, addr):
        self.sock.connect(addr)
        self.prints()

    def writeln(self, fmt, *args):
        s = fmt.format(*args) + '\r\n'
        self.sock.send(s.encode())
        return s

    def writeln2(self, fmt, *args, **kwargs):
        s2 = self.writeln(fmt, *args)
        s = s2
        hide_input = kwargs.get('hide_input', False)
        if hide_input:
            s = '*' * len(s) + '\r\n'
        print('>', s, end='')
        p = self.prints(kwargs.get('timeout', 0))
        return (s2, p)

    def recvall(self, timeout=0):
        data = b''
        self.sock.settimeout(timeout)
        try:
            while True:
                part = self.sock.recv(4096)
                data += part
        except:
            pass
        self.sock.settimeout(None)
        return data

    def prints(self, timeout=0):
        answ = self.recvall(timeout) if timeout else self.sock.recv(4096)
        to_print = answ.decode('utf8', 'ignore')
        print(to_print, end='')
        return to_print

    def starttls(self):
        self.sock = ssl.wrap_socket(
            self.sock, ssl_version=ssl.PROTOCOL_SSLv23
        )


if __name__ == '__main__':
    pass
    #main()
